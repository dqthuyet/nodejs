const func = require('./math.js');
describe('Math', () => {
  it('1 + 2 = 3', () => {
    expect(func(1, 2)).toEqual(3);
  });
});
